Simple lock-free containers written in C++ and Go.

# C++ #

* **Queue:** Lock-free Michael & Scott queue with deferred 'garbage collector' and storage of trash.

# Go #
* **Queue:** Lock-free Michael & Scott queue.
* **Stack:** Lock-free Treiber stack