package stack

import (
	"sync/atomic"
	"unsafe"
	"runtime"
)


type node struct {
	value interface{}
	next * node
}


type stack struct {
	head * node
	sz uint64
}

func NewStack() * stack {
	s := new(stack)
	s.head = new(node)
	s.sz = 0

	return s
}


func (s * stack) Push(val interface{}) {
	new_node := new(node)
	new_node.value = val

	for {
		new_node.next = s.head

		if atomic.CompareAndSwapPointer(
			(*unsafe.Pointer)(unsafe.Pointer(&s.head)), (unsafe.Pointer)(unsafe.Pointer(new_node.next)), unsafe.Pointer(new_node)) {
			s.sz++
			break
		}

		//back-off
		runtime.Gosched()
	}
}


func (s * stack) Pop() (interface{}) {

	res := s.head
	for {
		if res == nil {
			return nil
		}

		if atomic.CompareAndSwapPointer(
			(*unsafe.Pointer)(unsafe.Pointer(&s.head)), (unsafe.Pointer)(unsafe.Pointer(res)), unsafe.Pointer(res.next)) {
			s.sz--
			break
		}

		//back-off
		runtime.Gosched()
	}

	return res.value
}


func (s * stack) Size() (uint64) {
	return s.sz
}
