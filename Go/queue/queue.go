package queue

import (
	"sync/atomic"
	"unsafe"
	"runtime"
)


type node struct {
	value interface{}
	next * node
}


type queue struct {
	head * node
	tail * node
	sz uint64
}


func NewQueue() * queue {
	q := new(queue)
	q.head = new(node)
	q.tail = q.head
	q.sz = 0

	return q
}


func (q * queue) Push_back(val interface{}) {
	new_node := new(node)
	new_node.next = nil
	new_node.value = val

	local_tail := q.tail
	local_tail_next := local_tail.next
	for {
		local_tail = q.tail
		local_tail_next = local_tail.next

		if local_tail_next != nil {
			continue
		}

		res := atomic.CompareAndSwapPointer(
			(*unsafe.Pointer)(unsafe.Pointer(&local_tail.next)), (unsafe.Pointer)(unsafe.Pointer(local_tail_next)), unsafe.Pointer(new_node))

		if res == true {
			q.sz++
			break
		}

		//back-off
		runtime.Gosched()
	}

	atomic.CompareAndSwapPointer((*unsafe.Pointer)(unsafe.Pointer(&q.tail)), (unsafe.Pointer)(unsafe.Pointer(local_tail)), unsafe.Pointer(new_node))
}


func (q * queue) Pop_front() (interface{}) {
	for {
		local_head := q.head
		local_head_next := local_head.next

		if local_head != q.head {
			continue
		}

		if local_head_next == nil {
			return nil
		}

		if atomic.CompareAndSwapPointer(
			(*unsafe.Pointer)(unsafe.Pointer(&q.head)), (unsafe.Pointer)(unsafe.Pointer(local_head)), unsafe.Pointer(local_head_next)) {
			q.sz--
			return local_head_next.value
		}

		//back-off
		runtime.Gosched()
	}
}


func (q * queue) Size() (uint64) {
    return q.sz
}