#ifndef QUEUE_HPP
#define QUEUE_HPP

#include <memory>
#include <thread>
#include <atomic>



/**
 * @brief The backoff class
 * for avoid livelocks
 */
class backoff {
public:

    static void pause()
    {
        __asm__ volatile (
                    "rep;"
                    "nop;"
                    );
    }


    static void yield()
    {
        std::this_thread::yield();
    }

};



template<typename T> class Queue {
public:

    /**
     * @brief Queue constructor
     * @param water_mark wm for thrash storage.
     * Defer GC starts when storage.size() >= water_mark.
     *
     * Default value == std::thread::hardware_concurrency()*2
     */
    Queue(const unsigned water_mark = 0): head_(new struct node()), tail_(head_.load()) {
        if(water_mark) {
            water_mark_old_data_sz.store(water_mark, std::memory_order_relaxed);
        }
    }



    /// deleted ///
    Queue(const Queue<T>& copy) = delete;
    Queue(Queue<T>&& copy) = delete;
    Queue& operator = (const Queue& copy) = delete;
    Queue& operator = (Queue<T>&& copy) = delete;



    ~Queue() {
        clear();

        struct node * p = head_.load(std::memory_order_relaxed);
        if(p)
            delete p;
    }



    /**
     * @brief push_back add data to the end of the queue
     * Copy constructor of T will be called
     * @param val data
     */
    void push_back(const T& val) noexcept {
        struct node * new_node = new struct node(val);
        __push_back(new_node);
    }



    /**
     * @brief push_back add data to the end of the queue
     * Move constructor of T will be called
     * @param val data
     */
    void push_back(T&& val) noexcept {
        struct node * new_node = new struct node(std::forward<T>(val));
        __push_back(new_node);
    }



    /**
     * @brief pop_front remove first element from the queue
     * @return shared pointer to the data
     */
    std::shared_ptr<T> pop_front() noexcept {
        threads_counter_.fetch_add(1, std::memory_order_acq_rel);

        std::shared_ptr<T> res;
        while(true) {
            struct node * local_head = head_.load(std::memory_order_relaxed);
            struct node * local_head_next = local_head->next_.load(std::memory_order_relaxed);

            if(local_head != head_.load(std::memory_order_relaxed))
                continue;

            if(!local_head_next) {
                threads_counter_.fetch_sub(1, std::memory_order_relaxed);
                return res;
            }

            node * local_tail = tail_.load(std::memory_order_acquire);
            if(local_tail == local_head) {
                tail_.compare_exchange_strong(local_tail, local_head_next, std::memory_order_release, std::memory_order_acquire);
                continue;
            }

            //assign new head
            if(head_.compare_exchange_strong(local_head, local_head_next, std::memory_order_release, std::memory_order_acquire)) {
                res = local_head_next->data_;
                try_defer_erase(local_head);
                sz_.fetch_sub(1, std::memory_order_relaxed);
                break;
            }

            //if head_.CAS(...) failed (livelock)
            backoff::pause();
        }

        return res;
    }



    /**
     * @brief clear erase all the elements from the queue
     * and deallocate the memory
     * @WARN this function isn't thread-safe!
     */
    void clear() noexcept
    {
        struct node * local_head = head_.load(std::memory_order_relaxed);
        struct node * local_head_next = local_head->next_;

        if(!local_head_next) {
            local_head->is_stuff_.exchange(true, std::memory_order_relaxed);
            return;
        }

        //elements in the queue
        struct node * p = nullptr;
        do {
            p = head_.load(std::memory_order_relaxed)->next_.exchange(
                        head_.load(std::memory_order_relaxed)->next_.load(std::memory_order_relaxed)->next_, std::memory_order_relaxed);
            if(p->data_)
                p->data_.reset();
            delete p;
            sz_.fetch_sub(1, std::memory_order_relaxed);
            p = head_.load()->next_.load(std::memory_order_relaxed);
        }
        while(p);

        //GC
        struct node * head_thrash_nodes_ = thrash_nodes_.load(std::memory_order_relaxed);
        while(head_thrash_nodes_) {
            struct node * tmp_node = head_thrash_nodes_->next_.load(std::memory_order_relaxed);
            delete head_thrash_nodes_;
            head_thrash_nodes_ = tmp_node;
            old_data_sz_.fetch_sub(1, std::memory_order_relaxed);
        }

        head_.load()->is_stuff_.exchange(true, std::memory_order_relaxed);
        tail_.exchange(head_, std::memory_order_relaxed);
    }



    /**
     * @brief front return first element of the queue
     * @return shared pointer to the data
     */
    std::shared_ptr<T> front() const noexcept
    {
        struct node * local_head = head_.load(std::memory_order_relaxed);
        struct node * local_head_next = local_head->next_.load(std::memory_order_relaxed);

        //if Queue is emtpy
        if(!local_head_next) {
            return std::shared_ptr<T>();
        }

        if(head_.load()->is_stuff_) {
            return local_head_next->data_;
        }

        return local_head->data_;
    }



    /**
     * @brief back return last element of the queue
     * @return shared pointer to the data
     */
    std::shared_ptr<T> back() const noexcept
    {
        struct node * local_head = head_.load(std::memory_order_relaxed);
        struct node * local_head_next = local_head->next_.load(std::memory_order_relaxed);
        struct node * local_tail = tail_.load(std::memory_order_relaxed);

        //if Queue is emtpy
        if(!local_head_next) {
            return std::shared_ptr<T>();
        }

        if(local_tail == local_head) {
            return local_head_next->data_;
        }

        return local_tail->data_;
    }




    /**
     * @brief size return the number of elements in the queue
     * @WARN the result is only accurate. The other thread can modify queue
     * @return the number of elements
     */
    size_t size() const noexcept
    {
        return sz_.load(std::memory_order_relaxed);
    }




    /**
     * @brief empty check if the queue is empty
     * @WARN the result is only accurate. The other thread can modify queue
     * @return true is the queue is empty, false otherwise
     */
    bool empty() const noexcept
    {
        struct node * local_head = head_.load(std::memory_order_relaxed);
        struct node * local_head_next = local_head->next_.load(std::memory_order_relaxed);

        if(!local_head_next) {
            return true;
        }

        return false;
    }


private:
    /**
     * @brief The node struct
     * wrapper for the user data
     */
    struct node {
        std::atomic_bool is_stuff_{false};
        std::shared_ptr<T> data_;
        std::atomic<node * > next_{nullptr};

        node() { is_stuff_.store(true); }
        node(const T& val): data_(std::make_shared<T>(val)), next_(nullptr) {;}
        node(T&& val): data_(std::make_shared<T>(std::forward<T>(val))), next_(nullptr) {;}
        ~node() {;}
    };



    /**
     * @brief defer_add_obj_to_storage add old object to the thrash storage
     * @param obj old object
     * @param nodes_to_del pointer to the thrash storage
     */
    void defer_add_obj_to_storage(struct node * obj, struct node * nodes_to_del) noexcept {
        struct node * thrash_nodes_tail = nodes_to_del;
        if(nodes_to_del) {
            while(nodes_to_del->next_.load(std::memory_order_relaxed))
                nodes_to_del = nodes_to_del->next_.load(std::memory_order_relaxed);
            nodes_to_del->next_ = obj;
            nodes_to_del = nodes_to_del->next_.load(std::memory_order_relaxed);
        }
        else {
            nodes_to_del = obj;
            thrash_nodes_tail = nodes_to_del;
        }

        struct node * local_thrash_nodes = nullptr;
        do {
            nodes_to_del->next_ = thrash_nodes_.load(std::memory_order_acquire);
            local_thrash_nodes = nodes_to_del->next_;
        }
        while(!thrash_nodes_.compare_exchange_weak(
                  local_thrash_nodes, thrash_nodes_tail, std::memory_order_release, std::memory_order_acquire));

        old_data_sz_.fetch_add(1, std::memory_order_relaxed);
    }



    /**
     * @brief try_defer_erase defer garbage collector
     * @param obj old object
     */
    void try_defer_erase(struct node * obj) noexcept {
        if(threads_counter_.load(std::memory_order_acquire) == 1) {
            struct node * nodes_to_del = thrash_nodes_.exchange(nullptr, std::memory_order_relaxed);
            if(threads_counter_.fetch_sub(1, std::memory_order_acq_rel) <= 1) {
                //if storage.size() >= water_mark of that storage we should deallocate memory
                if(old_data_sz_.load(std::memory_order_relaxed) >= water_mark_old_data_sz.load(std::memory_order_relaxed)) {
                    const int wm_old_data_sz_ = old_data_sz_.load(std::memory_order_acquire)-water_mark_old_data_sz.load(std::memory_order_relaxed)/2;
                    for(int i=0; i<wm_old_data_sz_ && nodes_to_del; ++i) {
                        struct node * tmp_node = nodes_to_del->next_.load(std::memory_order_relaxed);
                        delete nodes_to_del;
                        nodes_to_del = tmp_node;
                        old_data_sz_.fetch_sub(1, std::memory_order_acq_rel);
                    }
                }
            }

            //add object to the storage
            defer_add_obj_to_storage(obj, nodes_to_del);
        }
        //we cannot deallocate memory because the other thread can modify the storage
        else {
            struct node * new_thrash_nodes = nullptr;
            do {
                obj->next_ = thrash_nodes_.load(std::memory_order_acquire);
                new_thrash_nodes = obj->next_;
            }
            while(!thrash_nodes_.compare_exchange_weak(new_thrash_nodes, obj, std::memory_order_acq_rel));
            old_data_sz_.fetch_add(1, std::memory_order_relaxed);
            threads_counter_.fetch_sub(std::memory_order_acq_rel);
        }
    }



    /**
     * @brief __push_back add data to the end of the queue
     * @param new_node object to add
     */
    void __push_back(struct node * new_node) noexcept {
        struct node * local_tail = nullptr;

        while(true) {
            local_tail = tail_.load(std::memory_order_acquire);
            struct node * local_tail_next = local_tail->next_.load(std::memory_order_acquire);

            if(local_tail_next != nullptr) {
               tail_.compare_exchange_strong(local_tail, local_tail_next, std::memory_order_release, std::memory_order_acquire);
               continue;
            }

            struct node * tmp = nullptr;
            if(tail_.load()->next_.compare_exchange_weak(tmp, new_node, std::memory_order_release, std::memory_order_acquire)) {
                break;
            }

            //if tail_.CAS(...) failed (livelock)
            backoff::yield();
        }
        tail_.compare_exchange_strong(local_tail, new_node, std::memory_order_acq_rel);
        sz_.fetch_add(1, std::memory_order_relaxed);
    }



    //head and tail of the queue
    std::atomic<node *> head_{nullptr};
    std::atomic<node *> tail_{nullptr};

    //counter of the threads in pop() function
    std::atomic<int> threads_counter_{0};

    //storage for the old data
    std::atomic<node *> thrash_nodes_{nullptr};

    //current size of this storage
    std::atomic<unsigned> old_data_sz_{0};

    //water mark for this storage (for defer GC call)
    std::atomic<unsigned> water_mark_old_data_sz{std::thread::hardware_concurrency()*2};

    //size of this queue
    std::atomic<int> sz_{0};
};

#endif // QUEUE_HPP
