#include <iostream>
#include <thread>
#include <vector>

#include "queue.hpp"


std::atomic_int producer_count(0);
std::atomic_int consumer_count(0);

const int iterations = 1000000;
const int producer_thread_count = 4;
const int consumer_thread_count = 4;

Queue<int> lst(16);

std::atomic<bool> done{false};
void producer(void)
{
    for (int i = 0; i != iterations; ++i) {
        int value = ++producer_count;
        lst.push_back(value);
    }
}

void consumer(void)
{
    while (!done) {
        std::shared_ptr<int> i = lst.pop_front();
        if(!i)
            continue;
        ++consumer_count;
    }

    while (lst.size()) {
        std::shared_ptr<int> i = lst.pop_front();
        if(!i)
            break;
        ++consumer_count;
    }
}




int main()
{

    std::vector<std::thread> v;
    v.reserve(8);
    
    auto t1 = std::chrono::high_resolution_clock::now();

    for(int i=0; i<8; ++i) {
        if(i<4) {
            v.push_back(std::move(std::thread(producer)));
        }
        else {
            v.push_back(std::move(std::thread(consumer)));
        }
    }

    for(int i=0; i<4; ++i) {
        v[i].join();
    }

    done = true;

    for(int i=4; i<8; ++i) {
        v[i].join();
    }
    
    auto t2 = std::chrono::high_resolution_clock::now();

    std::cout << "duration: " << (std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()) << " milliseconds." << std::endl;
    std::cout << "produced " << producer_count << " objects." << std::endl;
    std::cout << "consumed " << consumer_count << " objects." << std::endl;
    
    return 0;
}

